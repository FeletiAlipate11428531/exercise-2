package com.id11612672.exercise2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by SD on 2014/8/5.
 */
public class ActivityTwo extends Activity {
    /*
     *global variables
     */
    private String sub_name;
    private String sub_num;
    private String type;
    private TextView sName2;
    private TextView sNum2;
    private TextView type2;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_two);
        /*
         *get the strings from activity one
         */
        Intent intent = getIntent();
        sub_name = intent.getStringExtra(Constant.SUB_NAME);
        sub_num = intent.getStringExtra(Constant.SUB_NUM);
        type = intent.getStringExtra(Constant.TYPE);

        TextView sName2 = (TextView)findViewById(R.id.sname2);
        TextView sNum2 = (TextView)findViewById(R.id.snum2);
        TextView type2= (TextView)findViewById(R.id.type2);
        /*
        * set the strings to textviews
        * */
        sName2.setText(sub_name);
        sNum2.setText(sub_num);
        type2.setText(type);

        Button btnPop = (Button)findViewById(R.id.btnPop);
        btnPop.setOnClickListener(new btnPop());
    }
    private class btnPop implements View.OnClickListener{
        public void onClick(View v){
            /*
            * create a new popup and set the messages on it
            * */
            new AlertDialog.Builder(ActivityTwo.this)
            .setIcon(R.drawable.subject)
            .setTitle(R.string.subject)
            .setMessage(String.format(getString(R.string.format_subject),sub_name,sub_num,type))
            .setPositiveButton(R.string.buttonOK, null)
            .show();

        }
    }
}
