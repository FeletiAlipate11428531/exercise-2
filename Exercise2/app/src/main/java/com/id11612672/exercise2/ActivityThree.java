package com.id11612672.exercise2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by SD on 2014/8/7.
 */
public class ActivityThree extends Activity {
    private Button submit;
    private TextView random;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_three);
        submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                random = (TextView)findViewById(R.id.random);
                Intent in = new Intent();
                in.putExtra(Constant.RANDOM,random.getText().toString());
                setResult(RESULT_OK,in);
                //must finish after the result
                finish();
            }
        });
    }
}
