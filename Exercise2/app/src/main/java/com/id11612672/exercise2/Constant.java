package com.id11612672.exercise2;

/**
 * Created by SD on 2014/8/8.
 */
public class Constant {
    static final String SUB_NAME = "sub_name";
    static final String SUB_NUM = "sub_num";
    static final String TYPE = "type";
    static final String RANDOM = "random";
    static final int REQUEST_CODE = 999;
    static final String TAG = "MAD";
}
