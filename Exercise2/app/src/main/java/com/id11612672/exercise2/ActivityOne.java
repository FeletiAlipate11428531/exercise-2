package com.id11612672.exercise2;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class ActivityOne extends Activity {
    private Button showA1Btn;
    private Button showB1Btn;
    private TextView subName;
    private TextView subNum;

    private RadioGroup group;
    private RadioButton rb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_one);

        showA1Btn = (Button)findViewById(R.id.showA1);
        showA1Btn.setOnClickListener(new btnA1Listener());

        showB1Btn = (Button)findViewById(R.id.showB1);
        showB1Btn.setOnClickListener(new btnB1Listener());
    }
    private class btnA1Listener implements View.OnClickListener{
        @Override
        public void onClick(View view) {
            subName = (TextView)findViewById(R.id.editText1);
            subNum = (TextView)findViewById(R.id.editText2);
            String sName = subName.getText().toString();
            String sNum = subNum.getText().toString();

            group =(RadioGroup)findViewById(R.id.radioGroup);
            rb = (RadioButton)ActivityOne.this.findViewById(group.getCheckedRadioButtonId());

            if(sName.length() == 0){
                Toast.makeText(ActivityOne.this, R.string.snameerror,Toast.LENGTH_LONG).show();
                return;
            }

            Intent intent = new Intent(ActivityOne.this,ActivityTwo.class);
            intent.putExtra(Constant.SUB_NAME,sName);
            intent.putExtra(Constant.SUB_NUM,sNum);
            intent.putExtra(Constant.TYPE,rb.getText().toString());
            startActivity(intent);
            finish();
        }
    }

    private class btnB1Listener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ActivityOne.this,ActivityThree.class);
            startActivityForResult(intent,Constant.REQUEST_CODE);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent in){
        if(in != null) {
            if (requestCode == Constant.REQUEST_CODE) {
                String result = in.getStringExtra(Constant.RANDOM);
                Toast.makeText(this, result, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onStart(){
        super.onStart();
        Log.d(Constant.TAG,"In the onStart() event");
    }
    public void onRestart(){
        super.onStart();
        Log.d(Constant.TAG,"In the onRestart() event");
    }
    public void onResume(){
        super.onStart();
        Log.d(Constant.TAG,"In the onResume() event");
    }
    public void onPause(){
        super.onStart();
        Log.d(Constant.TAG,"In the onPause() event");
    }
    public void onStop(){
        super.onStart();
        Log.d(Constant.TAG,"In the onStop() event");
    }
    public void onDestroy(){
        super.onStart();
        Log.d(Constant.TAG,"In the onDestroy() event");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
